#include <ncurses.h>
#include <stdlib.h>
#include <time.h>
#include "form.h"

#define num(x) (sizeof(x) / sizeof(x[0]))
#define OUTFILE "results.txt"

void init_curses();
void exit_menu();
void save_results();

int y, x, score, res_y;

int main()
{
    init_curses(); 

    char *questions[25] = {"Feeling sad or down in the dumps",
                      "Feeling unhappy or blue",
                      "Crying spells or tearfulness",
                      "Feeling discouraged",
                      "Feeling hopeless",
                      "Low self-esteem",
                      "Feeling worthless or inadequate",
                      "Guilt or shame",
                      "Criticizing yourself or blaming others",
                      "Difficulty making decisions",
                      "Loss of interest in family, friends or colleagues",
                      "Loneliness",
                      "Spending less time with family or friends",
                      "Loss of motivation",
                      "Loss of interest in work or other activities",
                      "Avoiding work or other activities",
                      "Loss of pleasure or satisfaction in life",
                      "Feeling tired",
                      "Difficulty sleeping or sleeping too much",
                      "Decreased  or increased appetite",
                      "Loss of interest in sex",
                      "Worrying about your health",
                      "Do you have any suicidal thoughts?",
                      "Would you like to end your life?",
                      "Do you have a plan for harming yourself?"
                      };

    printw("Mark how much you have experienced each symptom during the past week, including today.\n");
    printw("0 = not at all, 1 = somewhat, 2 = moderately, 3 = a lot, 4 = extremely\n");
    printw("Use WASD or arrow keys. Use space or enter to select.\n");    
    printw("Press q to quit.");
    int n_of_q = num(questions);
    score = checklist(6, 4, questions, n_of_q, 5);
    
    res_y = 7+n_of_q;
    
    getmaxyx(stdscr, y, x);
    mvprintw(res_y, x/2-8, "Your score is %2d", score);
    if (score <= 5) 
        mvprintw(++res_y, x/2-7, "No depression.");
    else if (6 <= score && score <= 10)
        mvprintw(++res_y, x/2-9, "Normal but unhappy");
    else if (11 <= score && score <= 25)
        mvprintw(++res_y, x/2-7, "Mild depression");
    else if (26 <= score && score <= 50)
        mvprintw(++res_y, x/2-8, "Moderate depression");
    else if (51 <= score && score <= 75)
        mvprintw(++res_y, x/2-8, "Severe depression");
    else if (76 <= score && score <= 100)
        mvprintw(++res_y, x/2-9, "Extreme depression");

    curs_set(0);
    exit_menu();
    endwin();
    return 0;
}

void init_curses() 
{
    initscr();
    noecho();
    raw();
    keypad(stdscr, TRUE);
    cbreak();
}

void exit_menu()
{
    char *options[2] = {"Save", "Exit"};
    int choice, hl = 0;

    while (1) {
        for (int i = 0; i < 2; i++) {
            if (i == hl)
                wattron(stdscr, A_REVERSE);
            mvprintw(res_y+2, x/2-9+(i+1)*4+i, options[i]);
            wattroff(stdscr, A_REVERSE);
        }
        choice = getch();

        switch (choice) {
            case KEY_LEFT:
                if (hl > 0)
                    hl--;
                break;
            case KEY_RIGHT:
                if (hl < 2)
                    hl++;
                break;
            default:
                break;
        }
        if (choice == 10)
        break;
    }

    if (hl) 
        return;
    else 
        save_results();        
}

void save_results()
{
    FILE *fp = fopen(OUTFILE, "a+");
    time_t t = time(NULL);
    struct tm *tm = localtime(&t);
    char s[64];
    strftime(s, sizeof(s), "%c", tm);
    fprintf(fp, "%s\t%3d\n", s, score);
    fclose(fp);
    return;
}

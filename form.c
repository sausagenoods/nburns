#include <ncurses.h>
#include "form.h"

int checklist(int y, int x, char *questions[], int lines, int boxes)
{
    int col, row;
    getmaxyx(stdscr, col, row); // get term size
       
    for (int i = 0; i < lines; i++) {
        mvprintw(y+i, 1, "%2d-", i+1);
        mvprintw(y+i, 4, questions[i]);  
    }
                                                                       
    int len = 0, longest = 0;
    /* get the len of longest string */
    for (int j = 0; j < lines; j++) {
        for (int k = 0; questions[j][k] != '\0'; k++) {
            len++;
        }
        if (len > longest)
            longest = len;
        len = 0;
    }
    
    /* calculate the x pos and spaces between each checkbox */
    int spaces;
    spaces = (((row - x - longest) / boxes) < 5) ? spaces : 5;

    return checkbox(y, row - boxes*spaces, lines, boxes, spaces);
}

int checkbox(int y, int x, int lines, int boxes, int space)  // n is the number of checkboxes
{
    int i, k, c, y_pos, x_pos;
    
    /* Using 3d arrays for the job is not very convenient. For something proper see:
       https://gitlab.com/winterlinn/ncurses-checklist */
   
    int blanks[lines][boxes][2];

    for (int l = 0; l < 5; l++) 
        mvprintw(y-1, x+l*space+1, "%d", l);

    /* draw checkboxes */
    for (k = 0; k < lines; k++) {
        for (i = 0; i < boxes; i++) {
            mvprintw(y+k, x+i*space, "%c", '[');
            mvprintw(y_pos = y+k, x_pos = x+i*space+1, "%c", ' '); 
            blanks[k][i][0] = y_pos; // [0] holds the y pos
            blanks[k][i][1] = x_pos; // [1] holds the x pos
            mvprintw(y+k, x+i*space+2, "%c", ']');
        }
    }
      
    int line = 0, box = 0, score = 0;
    move(blanks[line][box][0], blanks[line][box][1]);

    while ((c = getch()) != 'q') {
        switch (c) {
            case KEY_UP:
            case 'W':
            case 'w':
                mvchgat(blanks[line][box][0], 0, -1, A_NORMAL, 1, NULL);
                if (line-1 >= 0)
                    line--;
                mvchgat(blanks[line][box][0], 0, -1, A_REVERSE, 1, NULL);
                break;
            
            case KEY_DOWN:
            case 'S':
            case 's': 
                mvchgat(blanks[line][box][0], 0, -1, A_NORMAL, 1, NULL);
                if (line+1 < lines)
                    line++;
                mvchgat(blanks[line][box][0], 0, -1, A_REVERSE, 1, NULL);
                break;
            
            case KEY_LEFT:
            case 'A':
            case 'a':
                if (box-1 >= 0)
                    box--;
                break;
            
            case KEY_RIGHT:
            case 'D':
            case 'd':
                if (box+1 < boxes)
                    box++;
                break;
           
            case 10:
            case ' ':
                for (int l = 0; l < boxes; l++) {
                    if (mvinch(blanks[line][l][0], blanks[line][l][1]) == 'x') {
                        mvprintw(blanks[line][l][0], blanks[line][l][1], "%c", ' ');
                        score -= l;
                    }
                }
                
                if (mvinch(blanks[line][box][0], blanks[line][box][1]) != 'x') {
                    mvprintw(blanks[line][box][0], blanks[line][box][1], "%c", 'x');
                    score += box;
                } 
                break;
                
            default:
                break;
        } 

        move(blanks[line][box][0], blanks[line][box][1]);
    }
    return score;
}

